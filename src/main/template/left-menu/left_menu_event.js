$(function(){
<?for(menu in menus){?>
    <? for(childMenu in menu.childrenMenu){?>
    <? var name = lowerTableName(childMenu.model);?>
    /**
     * 打开@{name}信息列表
	 * 当用户点击@{name}信息菜单项时，调用该函数 
	 * 菜单项目ID命名规范:@{childMenu.id}
	 */
	$('#@{childMenu.id}').parent().click(function(){
		$.ajax({
			type:"POST",
			cache: false,
			url:ctx+"/@{name}/list",
			data:"",
			success:function(msg){
				addPage(msg);
			},
			complete:function(){
				waitingStop();
			},
			beforeSend:function(){
				waiting();
			}
		});
	});	
	<?}?>
<?}?>
	
});



/**
 * 向工作区域添加面板
 */
function addPage(msg) {	
	$("#work_area").empty();
	$("#work_area").append(msg);
	scrollPane();	
}
