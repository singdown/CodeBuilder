package com.zzwtec.community.common;


/**
 * url请求常量配置
 * @author yangtonggan
 * @date 2016-04-01
 * 
 */

public interface UrlConstants {	
	    //映射--account列表--请求地址
		public static final String URL_ACCOUNT = "/account";
		//映射--添加数据account--请求地址
		public static final String URL_ACCOUNT_ADD = "/account/add";
		//映射--删除数据account--请求地址
		public static final String URL_ACCOUNT_DEL = "/account/del";
		//映射--修改数据account--请求地址
		public static final String URL_ACCOUNT_UPD = "/account/upd";
		//映射--添加account界面表单--请求地址
		public static final String URL_ACCOUNT_ADD_FORM_UI = "/account/add/form_ui";
		//映射--编辑account界面表单--请求地址
		public static final String URL_ACCOUNT_EDIT_FORM_UI = "/account/edit/form_ui";
	
	    //映射--account_right列表--请求地址
		public static final String URL_ACCOUNT_RIGHT = "/account_right";
		//映射--添加数据account_right--请求地址
		public static final String URL_ACCOUNT_RIGHT_ADD = "/account_right/add";
		//映射--删除数据account_right--请求地址
		public static final String URL_ACCOUNT_RIGHT_DEL = "/account_right/del";
		//映射--修改数据account_right--请求地址
		public static final String URL_ACCOUNT_RIGHT_UPD = "/account_right/upd";
		//映射--添加account_right界面表单--请求地址
		public static final String URL_ACCOUNT_RIGHT_ADD_FORM_UI = "/account_right/add/form_ui";
		//映射--编辑account_right界面表单--请求地址
		public static final String URL_ACCOUNT_RIGHT_EDIT_FORM_UI = "/account_right/edit/form_ui";
	
	    //映射--ad列表--请求地址
		public static final String URL_AD = "/ad";
		//映射--添加数据ad--请求地址
		public static final String URL_AD_ADD = "/ad/add";
		//映射--删除数据ad--请求地址
		public static final String URL_AD_DEL = "/ad/del";
		//映射--修改数据ad--请求地址
		public static final String URL_AD_UPD = "/ad/upd";
		//映射--添加ad界面表单--请求地址
		public static final String URL_AD_ADD_FORM_UI = "/ad/add/form_ui";
		//映射--编辑ad界面表单--请求地址
		public static final String URL_AD_EDIT_FORM_UI = "/ad/edit/form_ui";
	
	    //映射--ad_admin列表--请求地址
		public static final String URL_AD_ADMIN = "/ad_admin";
		//映射--添加数据ad_admin--请求地址
		public static final String URL_AD_ADMIN_ADD = "/ad_admin/add";
		//映射--删除数据ad_admin--请求地址
		public static final String URL_AD_ADMIN_DEL = "/ad_admin/del";
		//映射--修改数据ad_admin--请求地址
		public static final String URL_AD_ADMIN_UPD = "/ad_admin/upd";
		//映射--添加ad_admin界面表单--请求地址
		public static final String URL_AD_ADMIN_ADD_FORM_UI = "/ad_admin/add/form_ui";
		//映射--编辑ad_admin界面表单--请求地址
		public static final String URL_AD_ADMIN_EDIT_FORM_UI = "/ad_admin/edit/form_ui";
	
	    //映射--ad_admin_right列表--请求地址
		public static final String URL_AD_ADMIN_RIGHT = "/ad_admin_right";
		//映射--添加数据ad_admin_right--请求地址
		public static final String URL_AD_ADMIN_RIGHT_ADD = "/ad_admin_right/add";
		//映射--删除数据ad_admin_right--请求地址
		public static final String URL_AD_ADMIN_RIGHT_DEL = "/ad_admin_right/del";
		//映射--修改数据ad_admin_right--请求地址
		public static final String URL_AD_ADMIN_RIGHT_UPD = "/ad_admin_right/upd";
		//映射--添加ad_admin_right界面表单--请求地址
		public static final String URL_AD_ADMIN_RIGHT_ADD_FORM_UI = "/ad_admin_right/add/form_ui";
		//映射--编辑ad_admin_right界面表单--请求地址
		public static final String URL_AD_ADMIN_RIGHT_EDIT_FORM_UI = "/ad_admin_right/edit/form_ui";
	
	    //映射--ad_community列表--请求地址
		public static final String URL_AD_COMMUNITY = "/ad_community";
		//映射--添加数据ad_community--请求地址
		public static final String URL_AD_COMMUNITY_ADD = "/ad_community/add";
		//映射--删除数据ad_community--请求地址
		public static final String URL_AD_COMMUNITY_DEL = "/ad_community/del";
		//映射--修改数据ad_community--请求地址
		public static final String URL_AD_COMMUNITY_UPD = "/ad_community/upd";
		//映射--添加ad_community界面表单--请求地址
		public static final String URL_AD_COMMUNITY_ADD_FORM_UI = "/ad_community/add/form_ui";
		//映射--编辑ad_community界面表单--请求地址
		public static final String URL_AD_COMMUNITY_EDIT_FORM_UI = "/ad_community/edit/form_ui";
	
	    //映射--admdevice列表--请求地址
		public static final String URL_ADMDEVICE = "/admdevice";
		//映射--添加数据admdevice--请求地址
		public static final String URL_ADMDEVICE_ADD = "/admdevice/add";
		//映射--删除数据admdevice--请求地址
		public static final String URL_ADMDEVICE_DEL = "/admdevice/del";
		//映射--修改数据admdevice--请求地址
		public static final String URL_ADMDEVICE_UPD = "/admdevice/upd";
		//映射--添加admdevice界面表单--请求地址
		public static final String URL_ADMDEVICE_ADD_FORM_UI = "/admdevice/add/form_ui";
		//映射--编辑admdevice界面表单--请求地址
		public static final String URL_ADMDEVICE_EDIT_FORM_UI = "/admdevice/edit/form_ui";
	
	    //映射--admin列表--请求地址
		public static final String URL_ADMIN = "/admin";
		//映射--添加数据admin--请求地址
		public static final String URL_ADMIN_ADD = "/admin/add";
		//映射--删除数据admin--请求地址
		public static final String URL_ADMIN_DEL = "/admin/del";
		//映射--修改数据admin--请求地址
		public static final String URL_ADMIN_UPD = "/admin/upd";
		//映射--添加admin界面表单--请求地址
		public static final String URL_ADMIN_ADD_FORM_UI = "/admin/add/form_ui";
		//映射--编辑admin界面表单--请求地址
		public static final String URL_ADMIN_EDIT_FORM_UI = "/admin/edit/form_ui";
	
	    //映射--admin_right列表--请求地址
		public static final String URL_ADMIN_RIGHT = "/admin_right";
		//映射--添加数据admin_right--请求地址
		public static final String URL_ADMIN_RIGHT_ADD = "/admin_right/add";
		//映射--删除数据admin_right--请求地址
		public static final String URL_ADMIN_RIGHT_DEL = "/admin_right/del";
		//映射--修改数据admin_right--请求地址
		public static final String URL_ADMIN_RIGHT_UPD = "/admin_right/upd";
		//映射--添加admin_right界面表单--请求地址
		public static final String URL_ADMIN_RIGHT_ADD_FORM_UI = "/admin_right/add/form_ui";
		//映射--编辑admin_right界面表单--请求地址
		public static final String URL_ADMIN_RIGHT_EDIT_FORM_UI = "/admin_right/edit/form_ui";
	
	    //映射--admin_role列表--请求地址
		public static final String URL_ADMIN_ROLE = "/admin_role";
		//映射--添加数据admin_role--请求地址
		public static final String URL_ADMIN_ROLE_ADD = "/admin_role/add";
		//映射--删除数据admin_role--请求地址
		public static final String URL_ADMIN_ROLE_DEL = "/admin_role/del";
		//映射--修改数据admin_role--请求地址
		public static final String URL_ADMIN_ROLE_UPD = "/admin_role/upd";
		//映射--添加admin_role界面表单--请求地址
		public static final String URL_ADMIN_ROLE_ADD_FORM_UI = "/admin_role/add/form_ui";
		//映射--编辑admin_role界面表单--请求地址
		public static final String URL_ADMIN_ROLE_EDIT_FORM_UI = "/admin_role/edit/form_ui";
	
	    //映射--advertiser列表--请求地址
		public static final String URL_ADVERTISER = "/advertiser";
		//映射--添加数据advertiser--请求地址
		public static final String URL_ADVERTISER_ADD = "/advertiser/add";
		//映射--删除数据advertiser--请求地址
		public static final String URL_ADVERTISER_DEL = "/advertiser/del";
		//映射--修改数据advertiser--请求地址
		public static final String URL_ADVERTISER_UPD = "/advertiser/upd";
		//映射--添加advertiser界面表单--请求地址
		public static final String URL_ADVERTISER_ADD_FORM_UI = "/advertiser/add/form_ui";
		//映射--编辑advertiser界面表单--请求地址
		public static final String URL_ADVERTISER_EDIT_FORM_UI = "/advertiser/edit/form_ui";
	
	    //映射--area列表--请求地址
		public static final String URL_AREA = "/area";
		//映射--添加数据area--请求地址
		public static final String URL_AREA_ADD = "/area/add";
		//映射--删除数据area--请求地址
		public static final String URL_AREA_DEL = "/area/del";
		//映射--修改数据area--请求地址
		public static final String URL_AREA_UPD = "/area/upd";
		//映射--添加area界面表单--请求地址
		public static final String URL_AREA_ADD_FORM_UI = "/area/add/form_ui";
		//映射--编辑area界面表单--请求地址
		public static final String URL_AREA_EDIT_FORM_UI = "/area/edit/form_ui";
	
	    //映射--build列表--请求地址
		public static final String URL_BUILD = "/build";
		//映射--添加数据build--请求地址
		public static final String URL_BUILD_ADD = "/build/add";
		//映射--删除数据build--请求地址
		public static final String URL_BUILD_DEL = "/build/del";
		//映射--修改数据build--请求地址
		public static final String URL_BUILD_UPD = "/build/upd";
		//映射--添加build界面表单--请求地址
		public static final String URL_BUILD_ADD_FORM_UI = "/build/add/form_ui";
		//映射--编辑build界面表单--请求地址
		public static final String URL_BUILD_EDIT_FORM_UI = "/build/edit/form_ui";
	
	    //映射--cell列表--请求地址
		public static final String URL_CELL = "/cell";
		//映射--添加数据cell--请求地址
		public static final String URL_CELL_ADD = "/cell/add";
		//映射--删除数据cell--请求地址
		public static final String URL_CELL_DEL = "/cell/del";
		//映射--修改数据cell--请求地址
		public static final String URL_CELL_UPD = "/cell/upd";
		//映射--添加cell界面表单--请求地址
		public static final String URL_CELL_ADD_FORM_UI = "/cell/add/form_ui";
		//映射--编辑cell界面表单--请求地址
		public static final String URL_CELL_EDIT_FORM_UI = "/cell/edit/form_ui";
	
	    //映射--cell_band列表--请求地址
		public static final String URL_CELL_BAND = "/cell_band";
		//映射--添加数据cell_band--请求地址
		public static final String URL_CELL_BAND_ADD = "/cell_band/add";
		//映射--删除数据cell_band--请求地址
		public static final String URL_CELL_BAND_DEL = "/cell_band/del";
		//映射--修改数据cell_band--请求地址
		public static final String URL_CELL_BAND_UPD = "/cell_band/upd";
		//映射--添加cell_band界面表单--请求地址
		public static final String URL_CELL_BAND_ADD_FORM_UI = "/cell_band/add/form_ui";
		//映射--编辑cell_band界面表单--请求地址
		public static final String URL_CELL_BAND_EDIT_FORM_UI = "/cell_band/edit/form_ui";
	
	    //映射--comm_door_card列表--请求地址
		public static final String URL_COMM_DOOR_CARD = "/comm_door_card";
		//映射--添加数据comm_door_card--请求地址
		public static final String URL_COMM_DOOR_CARD_ADD = "/comm_door_card/add";
		//映射--删除数据comm_door_card--请求地址
		public static final String URL_COMM_DOOR_CARD_DEL = "/comm_door_card/del";
		//映射--修改数据comm_door_card--请求地址
		public static final String URL_COMM_DOOR_CARD_UPD = "/comm_door_card/upd";
		//映射--添加comm_door_card界面表单--请求地址
		public static final String URL_COMM_DOOR_CARD_ADD_FORM_UI = "/comm_door_card/add/form_ui";
		//映射--编辑comm_door_card界面表单--请求地址
		public static final String URL_COMM_DOOR_CARD_EDIT_FORM_UI = "/comm_door_card/edit/form_ui";
	
	    //映射--community列表--请求地址
		public static final String URL_COMMUNITY = "/community";
		//映射--添加数据community--请求地址
		public static final String URL_COMMUNITY_ADD = "/community/add";
		//映射--删除数据community--请求地址
		public static final String URL_COMMUNITY_DEL = "/community/del";
		//映射--修改数据community--请求地址
		public static final String URL_COMMUNITY_UPD = "/community/upd";
		//映射--添加community界面表单--请求地址
		public static final String URL_COMMUNITY_ADD_FORM_UI = "/community/add/form_ui";
		//映射--编辑community界面表单--请求地址
		public static final String URL_COMMUNITY_EDIT_FORM_UI = "/community/edit/form_ui";
	
	    //映射--door_card列表--请求地址
		public static final String URL_DOOR_CARD = "/door_card";
		//映射--添加数据door_card--请求地址
		public static final String URL_DOOR_CARD_ADD = "/door_card/add";
		//映射--删除数据door_card--请求地址
		public static final String URL_DOOR_CARD_DEL = "/door_card/del";
		//映射--修改数据door_card--请求地址
		public static final String URL_DOOR_CARD_UPD = "/door_card/upd";
		//映射--添加door_card界面表单--请求地址
		public static final String URL_DOOR_CARD_ADD_FORM_UI = "/door_card/add/form_ui";
		//映射--编辑door_card界面表单--请求地址
		public static final String URL_DOOR_CARD_EDIT_FORM_UI = "/door_card/edit/form_ui";
	
	    //映射--doorbell列表--请求地址
		public static final String URL_DOORBELL = "/doorbell";
		//映射--添加数据doorbell--请求地址
		public static final String URL_DOORBELL_ADD = "/doorbell/add";
		//映射--删除数据doorbell--请求地址
		public static final String URL_DOORBELL_DEL = "/doorbell/del";
		//映射--修改数据doorbell--请求地址
		public static final String URL_DOORBELL_UPD = "/doorbell/upd";
		//映射--添加doorbell界面表单--请求地址
		public static final String URL_DOORBELL_ADD_FORM_UI = "/doorbell/add/form_ui";
		//映射--编辑doorbell界面表单--请求地址
		public static final String URL_DOORBELL_EDIT_FORM_UI = "/doorbell/edit/form_ui";
	
	    //映射--enclosure列表--请求地址
		public static final String URL_ENCLOSURE = "/enclosure";
		//映射--添加数据enclosure--请求地址
		public static final String URL_ENCLOSURE_ADD = "/enclosure/add";
		//映射--删除数据enclosure--请求地址
		public static final String URL_ENCLOSURE_DEL = "/enclosure/del";
		//映射--修改数据enclosure--请求地址
		public static final String URL_ENCLOSURE_UPD = "/enclosure/upd";
		//映射--添加enclosure界面表单--请求地址
		public static final String URL_ENCLOSURE_ADD_FORM_UI = "/enclosure/add/form_ui";
		//映射--编辑enclosure界面表单--请求地址
		public static final String URL_ENCLOSURE_EDIT_FORM_UI = "/enclosure/edit/form_ui";
	
	    //映射--msg列表--请求地址
		public static final String URL_MSG = "/msg";
		//映射--添加数据msg--请求地址
		public static final String URL_MSG_ADD = "/msg/add";
		//映射--删除数据msg--请求地址
		public static final String URL_MSG_DEL = "/msg/del";
		//映射--修改数据msg--请求地址
		public static final String URL_MSG_UPD = "/msg/upd";
		//映射--添加msg界面表单--请求地址
		public static final String URL_MSG_ADD_FORM_UI = "/msg/add/form_ui";
		//映射--编辑msg界面表单--请求地址
		public static final String URL_MSG_EDIT_FORM_UI = "/msg/edit/form_ui";
	
	    //映射--property列表--请求地址
		public static final String URL_PROPERTY = "/property";
		//映射--添加数据property--请求地址
		public static final String URL_PROPERTY_ADD = "/property/add";
		//映射--删除数据property--请求地址
		public static final String URL_PROPERTY_DEL = "/property/del";
		//映射--修改数据property--请求地址
		public static final String URL_PROPERTY_UPD = "/property/upd";
		//映射--添加property界面表单--请求地址
		public static final String URL_PROPERTY_ADD_FORM_UI = "/property/add/form_ui";
		//映射--编辑property界面表单--请求地址
		public static final String URL_PROPERTY_EDIT_FORM_UI = "/property/edit/form_ui";
	
	    //映射--role列表--请求地址
		public static final String URL_ROLE = "/role";
		//映射--添加数据role--请求地址
		public static final String URL_ROLE_ADD = "/role/add";
		//映射--删除数据role--请求地址
		public static final String URL_ROLE_DEL = "/role/del";
		//映射--修改数据role--请求地址
		public static final String URL_ROLE_UPD = "/role/upd";
		//映射--添加role界面表单--请求地址
		public static final String URL_ROLE_ADD_FORM_UI = "/role/add/form_ui";
		//映射--编辑role界面表单--请求地址
		public static final String URL_ROLE_EDIT_FORM_UI = "/role/edit/form_ui";
	
	    //映射--systoken列表--请求地址
		public static final String URL_SYSTOKEN = "/systoken";
		//映射--添加数据systoken--请求地址
		public static final String URL_SYSTOKEN_ADD = "/systoken/add";
		//映射--删除数据systoken--请求地址
		public static final String URL_SYSTOKEN_DEL = "/systoken/del";
		//映射--修改数据systoken--请求地址
		public static final String URL_SYSTOKEN_UPD = "/systoken/upd";
		//映射--添加systoken界面表单--请求地址
		public static final String URL_SYSTOKEN_ADD_FORM_UI = "/systoken/add/form_ui";
		//映射--编辑systoken界面表单--请求地址
		public static final String URL_SYSTOKEN_EDIT_FORM_UI = "/systoken/edit/form_ui";
	
	    //映射--token_key列表--请求地址
		public static final String URL_TOKEN_KEY = "/token_key";
		//映射--添加数据token_key--请求地址
		public static final String URL_TOKEN_KEY_ADD = "/token_key/add";
		//映射--删除数据token_key--请求地址
		public static final String URL_TOKEN_KEY_DEL = "/token_key/del";
		//映射--修改数据token_key--请求地址
		public static final String URL_TOKEN_KEY_UPD = "/token_key/upd";
		//映射--添加token_key界面表单--请求地址
		public static final String URL_TOKEN_KEY_ADD_FORM_UI = "/token_key/add/form_ui";
		//映射--编辑token_key界面表单--请求地址
		public static final String URL_TOKEN_KEY_EDIT_FORM_UI = "/token_key/edit/form_ui";
	
	    //映射--user列表--请求地址
		public static final String URL_USER = "/user";
		//映射--添加数据user--请求地址
		public static final String URL_USER_ADD = "/user/add";
		//映射--删除数据user--请求地址
		public static final String URL_USER_DEL = "/user/del";
		//映射--修改数据user--请求地址
		public static final String URL_USER_UPD = "/user/upd";
		//映射--添加user界面表单--请求地址
		public static final String URL_USER_ADD_FORM_UI = "/user/add/form_ui";
		//映射--编辑user界面表单--请求地址
		public static final String URL_USER_EDIT_FORM_UI = "/user/edit/form_ui";
	
	    //映射--user_cell列表--请求地址
		public static final String URL_USER_CELL = "/user_cell";
		//映射--添加数据user_cell--请求地址
		public static final String URL_USER_CELL_ADD = "/user_cell/add";
		//映射--删除数据user_cell--请求地址
		public static final String URL_USER_CELL_DEL = "/user_cell/del";
		//映射--修改数据user_cell--请求地址
		public static final String URL_USER_CELL_UPD = "/user_cell/upd";
		//映射--添加user_cell界面表单--请求地址
		public static final String URL_USER_CELL_ADD_FORM_UI = "/user_cell/add/form_ui";
		//映射--编辑user_cell界面表单--请求地址
		public static final String URL_USER_CELL_EDIT_FORM_UI = "/user_cell/edit/form_ui";
	
	    //映射--user_set列表--请求地址
		public static final String URL_USER_SET = "/user_set";
		//映射--添加数据user_set--请求地址
		public static final String URL_USER_SET_ADD = "/user_set/add";
		//映射--删除数据user_set--请求地址
		public static final String URL_USER_SET_DEL = "/user_set/del";
		//映射--修改数据user_set--请求地址
		public static final String URL_USER_SET_UPD = "/user_set/upd";
		//映射--添加user_set界面表单--请求地址
		public static final String URL_USER_SET_ADD_FORM_UI = "/user_set/add/form_ui";
		//映射--编辑user_set界面表单--请求地址
		public static final String URL_USER_SET_EDIT_FORM_UI = "/user_set/edit/form_ui";
	
	    //映射--worker列表--请求地址
		public static final String URL_WORKER = "/worker";
		//映射--添加数据worker--请求地址
		public static final String URL_WORKER_ADD = "/worker/add";
		//映射--删除数据worker--请求地址
		public static final String URL_WORKER_DEL = "/worker/del";
		//映射--修改数据worker--请求地址
		public static final String URL_WORKER_UPD = "/worker/upd";
		//映射--添加worker界面表单--请求地址
		public static final String URL_WORKER_ADD_FORM_UI = "/worker/add/form_ui";
		//映射--编辑worker界面表单--请求地址
		public static final String URL_WORKER_EDIT_FORM_UI = "/worker/edit/form_ui";
	
		
}
