package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.Admdevice;
import com.zzwtec.community.model.AdmdeviceM;
import com.zzwtec.community.model.AdmdeviceMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AdmdeviceServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * Admdevice控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AdmdeviceController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_ADMDEVICE)
	public void list(){				
		//AdmdeviceServicePrx prx = IceServiceUtil.getService(AdmdeviceServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/admdevice_list.html");
	}
	
	/**
	 * 添加Admdevice
	 */
	@ActionKey(UrlConstants.URL_ADMDEVICE_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加Admdevice成功");;
		try{
			//AdmdeviceServicePrx prx = IceServiceUtil.getService(AdmdeviceServicePrx.class);
			Admdevice model = getBean(Admdevice.class);		
			//prx.addAdmdevice(model);				
		}catch(Exception e){
			repJson = new DataObject("添加Admdevice失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除Admdevice
	 */
	@ActionKey(UrlConstants.URL_ADMDEVICE_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除Admdevice成功");
		try{
			String ids = getPara("ids");		
			//AdmdeviceServicePrx prx = IceServiceUtil.getService(AdmdeviceServicePrx.class);
			//prx.delAdmdeviceByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除Admdevice失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_ADMDEVICE_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		Admdevice entity = new Admdevice();	
		setAttr("admdevice", entity);
		render("/system/view/admdevice_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_ADMDEVICE_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AdmdeviceServicePrx prx = IceServiceUtil.getService(AdmdeviceServicePrx.class);
			//AdmdeviceM modele = prx.inspectAdmdevice(id);
			Admdevice entity = new Admdevice();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("admdevice", entity);			
			render("/system/view/admdevice_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取Admdevice失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改Admdevice
	 */
	@ActionKey(UrlConstants.URL_ADMDEVICE_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新Admdevice成功");
		try{
			//AdmdeviceServicePrx prx = IceServiceUtil.getService(AdmdeviceServicePrx.class);
			Admdevice model = getBean(Admdevice.class);
			//prx.alterAdmdevice(model);
		}catch(Exception e){
			repJson = new DataObject("更新Admdevice失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AdmdeviceServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AdmdeviceMPage viewModel = prx.getAdmdeviceList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		Admdevice[] aray = new Admdevice[12];		
		for(int i=0;i<12;i++){
			aray[i] = new Admdevice();
			aray[i].id = "id-"+i;			
		}	
		List<Admdevice> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
