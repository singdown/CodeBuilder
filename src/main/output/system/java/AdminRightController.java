package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.AdminRight;
import com.zzwtec.community.model.AdminRightM;
import com.zzwtec.community.model.AdminRightMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.AdminRightServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * AdminRight控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class AdminRightController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_ADMIN_RIGHT)
	public void list(){				
		//AdminRightServicePrx prx = IceServiceUtil.getService(AdminRightServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/admin_right_list.html");
	}
	
	/**
	 * 添加AdminRight
	 */
	@ActionKey(UrlConstants.URL_ADMIN_RIGHT_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加AdminRight成功");;
		try{
			//AdminRightServicePrx prx = IceServiceUtil.getService(AdminRightServicePrx.class);
			AdminRight model = getBean(AdminRight.class);		
			//prx.addAdminRight(model);				
		}catch(Exception e){
			repJson = new DataObject("添加AdminRight失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除AdminRight
	 */
	@ActionKey(UrlConstants.URL_ADMIN_RIGHT_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除AdminRight成功");
		try{
			String ids = getPara("ids");		
			//AdminRightServicePrx prx = IceServiceUtil.getService(AdminRightServicePrx.class);
			//prx.delAdminRightByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除AdminRight失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_ADMIN_RIGHT_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		AdminRight entity = new AdminRight();	
		setAttr("adminRight", entity);
		render("/system/view/admin_right_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_ADMIN_RIGHT_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//AdminRightServicePrx prx = IceServiceUtil.getService(AdminRightServicePrx.class);
			//AdminRightM modele = prx.inspectAdminRight(id);
			AdminRight entity = new AdminRight();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("adminRight", entity);			
			render("/system/view/admin_right_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取AdminRight失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改AdminRight
	 */
	@ActionKey(UrlConstants.URL_ADMIN_RIGHT_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新AdminRight成功");
		try{
			//AdminRightServicePrx prx = IceServiceUtil.getService(AdminRightServicePrx.class);
			AdminRight model = getBean(AdminRight.class);
			//prx.alterAdminRight(model);
		}catch(Exception e){
			repJson = new DataObject("更新AdminRight失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(AdminRightServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//AdminRightMPage viewModel = prx.getAdminRightList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		AdminRight[] aray = new AdminRight[12];		
		for(int i=0;i<12;i++){
			aray[i] = new AdminRight();
			aray[i].id = "id-"+i;			
		}	
		List<AdminRight> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
