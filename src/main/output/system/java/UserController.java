package com.zzwtec.community.action.system;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.zzwtec.community.common.DataObject;
import com.zzwtec.community.common.IceServiceUtil;
import com.zzwtec.community.common.Page;
import com.zzwtec.community.common.UrlConstants;
import com.zzwtec.community.model.Area;
import com.zzwtec.community.model.BuildM;
import com.zzwtec.community.model.User;
import com.zzwtec.community.model.UserM;
import com.zzwtec.community.model.UserMPage;
import com.zzwtec.community.model.CommunityM;
import com.zzwtec.community.model.CommunityMPage;
import com.zzwtec.community.service.BuildServicePrx;
import com.zzwtec.community.service.UserServicePrx;
import com.zzwtec.community.service.CommunityServicePrx;

/**
 * User控制器
 * @author yangtonggan
 * @date  2016-04-01
 * 
 */
public class UserController extends Controller {
	/**
	 * 获取区域列表
	 */
	@ActionKey(UrlConstants.URL_USER)
	public void list(){				
		//UserServicePrx prx = IceServiceUtil.getService(UserServicePrx.class);
		//query(prx);
		// 构造测试数据	
		buildTestData();
		render("/system/view/user_list.html");
	}
	
	/**
	 * 添加User
	 */
	@ActionKey(UrlConstants.URL_USER_ADD)
	public void add(){		
		DataObject repJson = new DataObject("添加User成功");;
		try{
			//UserServicePrx prx = IceServiceUtil.getService(UserServicePrx.class);
			User model = getBean(User.class);		
			//prx.addUser(model);				
		}catch(Exception e){
			repJson = new DataObject("添加User失败");
		}finally{
			renderJson(repJson);
		}	
		
	}
	
	/**
	 * 删除User
	 */
	@ActionKey(UrlConstants.URL_USER_DEL)
	public void del(){		
		DataObject repJson = new DataObject("删除User成功");
		try{
			String ids = getPara("ids");		
			//UserServicePrx prx = IceServiceUtil.getService(UserServicePrx.class);
			//prx.delUserByIds(ids.split(","));
		}catch(Exception e){
			repJson = new DataObject("删除User失败");
		}finally{
			renderJson(repJson);
		}
	}
	
	/**
	 * 获取添加表单界面
	 */
	@ActionKey(UrlConstants.URL_USER_ADD_FORM_UI)
	public void addFormUI(){
		//TODO此处可以做一些预处理，比如为下拉列表赋值
		User entity = new User();	
		setAttr("user", entity);
		render("/system/view/user_add_form.html");
	}
	
	/**
	 * 根据ID获取一条记录,并且返回编辑界面UI
	 */
	@ActionKey(UrlConstants.URL_USER_EDIT_FORM_UI)
	public void editFormUI(){	
		try{			
			String id = getPara("id");	
			//UserServicePrx prx = IceServiceUtil.getService(UserServicePrx.class);
			//UserM modele = prx.inspectUser(id);
			User entity = new User();		
			//BeanUtils.copyProperties(modele, entity);	
			//TODO需要对界面下拉列表做预处理
			setAttr("user", entity);			
			render("/system/view/user_edit_form.html");
		}catch(Exception e){
			DataObject repJson = new DataObject("获取User失败");
			renderJson(repJson);
		}
	}	
	
	
	/**
	 * 修改User
	 */
	@ActionKey(UrlConstants.URL_USER_UPD)
	public void upd(){
		DataObject repJson = new DataObject("更新User成功");
		try{
			//UserServicePrx prx = IceServiceUtil.getService(UserServicePrx.class);
			User model = getBean(User.class);
			//prx.alterUser(model);
		}catch(Exception e){
			repJson = new DataObject("更新User失败");
		}finally{
			renderJson(repJson);
		}		
	}
	
	private void query(UserServicePrx prx){		
		Page page = new Page();		
		Map<String, String> term = new HashMap<String, String>();
		/*
		String name = getPara("name");
		term.put("name", name);
		//请参考注释代码设置查询条件
		*/
		//UserMPage viewModel = prx.getUserList(page, term);			
		//setAttr("viewData", viewModel);		
	}	
	
	private void buildTestData(){		
		User[] aray = new User[12];		
		for(int i=0;i<12;i++){
			aray[i] = new User();
			aray[i].id = "id-"+i;			
		}	
		List<User> pageList = Arrays.asList(aray);		
		setAttr("pageList", pageList);		
	}
	
}
