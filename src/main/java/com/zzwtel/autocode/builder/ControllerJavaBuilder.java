package com.zzwtel.autocode.builder;

import com.zzwtel.autocode.beetl.JavaFileTemplate;
import com.zzwtel.autocode.template.model.ControllerModel;

/**
 * java代码Controller构造器
 * @author yangtonggan
 * @date 2016-3-7
 */
public class ControllerJavaBuilder {

	/**
	 * 输入模型和模板，获取对应的java代码
	 * @param m
	 * @param modularDir 模块路径
	 */
	public void makeCode(ControllerModel m,String modularDir){
		JavaFileTemplate jft = new JavaFileTemplate();
		jft.generate(m, modularDir);
	}
}
