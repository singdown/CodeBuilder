package com.zzwtel.autocode.builder;

import com.zzwtel.autocode.beetl.EditFormJsTemplate;
import com.zzwtel.autocode.template.model.UIModel;

/**
 * 编辑表单js文件构造器，用于生成xxx_edit_form.js文件
 * @author yangtonggan
 * @date 2016-3-7
 */
public class EditFormJsBuilder {

	public void makeCode(UIModel vm,String modularDir) {
		EditFormJsTemplate template = new EditFormJsTemplate();
		template.generate(vm, modularDir);
		
	}

}
