package com.zzwtel.autocode.template.model;

import com.zzwtel.autocode.template.constants.ViewType;

/**
 * 属性名称
 * @author yangtonggan
 *<?xml version='1.0' encoding='UTF-8'?>
  <table name="account" model="account" class="com.pointercn.community.model.AccountModel">
	<property column="id" name="id" viewType="hidden" key="true"/>
	<property column="area_id" name="areaId" viewType="text" />
	<property column="area" name="area" type="text" />
	<property column="role_id" name="roleId" type="text" />
	<property column="name" name="name" type="text" />
	<property column="psw" name="psw" type="text" />
	<property column="level" name="level" type="text" />
   </table>
 *
 */
public class Property {
	//字段名
	private String column;
	//属性名
	private String name;
	//显示类型（参考bootstrap控件类型）
	private ViewType viewType;
	//是否主键
	private boolean key;
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ViewType getViewType() {
		return viewType;
	}
	public void setViewType(ViewType viewType) {
		this.viewType = viewType;
	}
	public boolean isKey() {
		return key;
	}
	public void setKey(boolean key) {
		this.key = key;
	}
	
}
