package com.zzwtel.autocode.db;

import java.sql.Connection;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DataSourceUtil {
	private static HikariDataSource ds;
	public static HikariDataSource getDataSource(){
		if(ds == null){
			HikariConfig config = new HikariConfig("/conf/hikari.properties");
			ds = new HikariDataSource(config);		
		}		
		return ds;
	}
	
	public static void main(String[] args){
		try{
			HikariDataSource ds = getDataSource();
			Connection con = ds.getConnection();
			System.out.println("con"+con);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
	}
	
}
