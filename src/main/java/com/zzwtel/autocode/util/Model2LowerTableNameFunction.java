package com.zzwtel.autocode.util;

import org.beetl.core.Context;
import org.beetl.core.Function;
import com.zzwtel.autocode.util.HumpUtil;

/**
 * model-->entity_model
 * @author yangtonggan
 * @date 2016-6-8
 */
public class Model2LowerTableNameFunction implements Function{

	public Object call(Object[] paras, Context ctx) {
		try{
			String param = (String)paras[0];
			String name = HumpUtil.underscoreName(param);	
			String entity_model = HumpUtil.toLowerCase(name);
			return entity_model;
		}catch(Exception e){
			throw new RuntimeException(e);
		}	
	}

}
